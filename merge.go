package merge

import (
	"encoding/json"
	"os"
	"os/exec"
	"strconv"
	"strings"

	"github.com/kpango/glg"
)

type InputData struct {
	Info Info `json:"info"`
}

type Info struct {
	OutputName string    `json:"output_name"`
	Streams    []Streams `json:"streams"`
}

type Streams struct {
	Filename    string `json:"filename"`
	ContentType string `json:"content_type"`
	Language    string `json:"language"`
}

func MergeStream(respInput interface{}, srcPath, dstPath string) error {

	var inputData InputData
	infoByte, err := json.Marshal(respInput)
	if err != nil {
		glg.Fatalln(err)
	}

	err = json.Unmarshal(infoByte, &inputData)
	if err != nil {
		glg.Fatalln(err)
	}

	streams := inputData.Info.Streams

	glg.Info("start to merge content:", streams)
	command := []string{"-y"}

	var videoStreams []Streams
	var audioStreams []Streams

	for _, stream := range streams {
		if stream.ContentType == "video" {
			videoStreams = append(videoStreams, stream)
		}
		if stream.ContentType == "audio" {
			audioStreams = append(audioStreams, stream)
		}
	}

	var copyVideos []string
	var copyAudios []string
	var i int

	for index, videoStream := range videoStreams {
		command = append(command, "-i", srcPath+videoStream.Filename)
		copyVideos = append(copyVideos, "-map", strconv.Itoa(index))
		i = index
	}

	for index, audioStream := range audioStreams {
		command = append(command, "-i", srcPath+audioStream.Filename)
		copyAudios = append(copyAudios, "-map", strconv.Itoa(index+1+i), "-c", "copy", "-bsf:a", "aac_adtstoasc", "-metadata:s:a:"+strconv.Itoa(index), "language="+audioStream.Language)
	}

	command = append(command, "-c", "copy")
	for _, copyVideo := range copyVideos {
		command = append(command, copyVideo)
	}

	for _, copyAudios := range copyAudios {
		command = append(command, copyAudios)
	}

	command = append(command, dstPath+inputData.Info.OutputName)

	glg.Debug(strings.Join(command[:], " "))

	cmd := exec.Command("ffmpeg", command...)
	//cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return err
	}

	glg.Info("merge content success and output to", dstPath+inputData.Info.OutputName)

	return nil

}
